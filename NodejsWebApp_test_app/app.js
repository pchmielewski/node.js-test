﻿var express = require("express"),
    mongoose = require("mongoose"),
    bodyParser = require("body-parser");

var app = express();

var port = process.env.port || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var db = mongoose.connect("mongodb://192.168.254.130/Books");

/*
var MongoClient = require('mongodb').MongoClient
    , format = require('util').format;
MongoClient.connect('mongodb://192.168.254.130/Books', function (err, db) {
    if (err) {
        throw err;
    } else {
        console.log("successfully connected to the database");
    }
    db.close();
});*/

var Book = require('./models/bookModel');

bookRouter = require("./Routes/bookRoutes")(Book);

app.use("/api/books", bookRouter);
//app.use("/api/authors", authorRouter);



app.get('/', function (req, res) { 
    res.send("Welcome to my app");
});

app.listen(port, function () {
    console.log("Gulp on PORT:" + port);
})

